from django.db import models
from django.contrib.auth.models import User

class Tuit(models.Model):
	id = models.AutoField(db_column = 'id', primary_key = True)
	mensaje = models.CharField(db_column = 'mensaje', max_length = 255)
	usuario = models.ForeignKey(User, db_column = 'usuario')
	fecha = models.DateTimeField(db_column = 'fecha')
	class Meta:
		db_table = 'tuit'


class Favorito(models.Model):
	id = models.AutoField(db_column = 'id', primary_key = True)
	fecha = models.DateTimeField(db_column = 'fecha')
	tuit = models.ForeignKey(Tuit, db_column = 'tuit')
	usuario = models.ForeignKey(User, db_column = 'usuario')
	class Meta:
		db_table = 'favorito'
		unique_together = ('tuit', 'usuario')

class Seguimiento(models.Model):
	id = models.AutoField(db_column= 'id', primary_key = True)
	seguido = models.ForeignKey(User,db_column = 'seguido', related_name = 'seguido_related')
	seguidor = models.ForeignKey(User, db_column = 'seguidor', related_name = 'seguidor_related')
	fecha = models.DateTimeField (db_column = 'fecha')
	class Meta:
		db_table = 'seguimiento'
		unique_together = ('seguido','seguidor')

class Retuit(models.Model):
	id = models.AutoField(db_column = 'id', primary_key = True)
	usuario = models.ForeignKey(User, db_column= 'usuario')
	tuit = models.ForeignKey(Tuit, db_column ='tuit')
	fecha = models.DateTimeField(db_column = 'fecha')
	class Meta:
		db_table = 'retuit'
		unique_together = ('usuario','tuit')



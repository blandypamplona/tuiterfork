from django.conf.urls import patterns, include, url
import usuario.views
import perfil.views

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'tuiter.views.home', name='home'),
    # url(r'^tuiter/', include('tuiter.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^$', usuario.views.home),
    url(r'^usuario/login/$', usuario.views.login),
	url(r'^usuario/logout/$', usuario.views.logout),
    url(r'^perfil/$', perfil.views.home)
)
